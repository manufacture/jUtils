package ru.manufacture.util.io;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import org.dom4j.Document;
import org.junit.Assert;
import org.junit.Test;

public class XmlTest extends Assert {

    @Test
    public void testValidate() throws Exception {
        Path xmlPath = Paths.get("src\\test\\resources\\process[1].xml");
        final Path xsdPath = Paths.get("src\\test\\resources\\process.xsd");
        Source xml = new StreamSource(new FileInputStream(xmlPath.toFile()));
        Source xsd = new StreamSource(new FileInputStream(xsdPath.toFile()));
        Xml.validate(xml, xsd);

        Document document = Xml.read(xmlPath);
        System.out.println(document.asXML());
    }
}