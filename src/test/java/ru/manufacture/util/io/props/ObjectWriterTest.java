package ru.manufacture.util.io.props;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.manufacture.types.format.EnumFormat;
import ru.manufacture.types.format.Format;

public class ObjectWriterTest extends Assert {
    enum Gender {male, female}

    public static class Human {
        private Gender gender;
        private String name;
        private Date birthDate;

        public Human() {
        }

        public Human(Gender gender, String name, Date birthDate) {
            this.gender = gender;
            this.name = name;
            this.birthDate = birthDate;
        }

        @Override
        public String toString() {
            return "HUMAN{" +
                    "gender=" + gender +
                    ", name='" + name + '\'' +
                    ", birthDate=" + birthDate +
                    '}';
        }
    }

    public static class Man extends Human {
        private String options;

        public Man() {
        }

        public Man(String name, Date birthDate, String options) {
            super(Gender.male, name, birthDate);
            this.options = options;
        }

        @Override
        public String toString() {
            return "Man{" +
                    "options='" + options + '\'' +
                    "} " + super.toString();
        }
    }

    public static class Woman extends Human {
        private String options;

        public Woman() {
        }

        public Woman(String name, Date birthDate, String options) {
            super(Gender.female, name, birthDate);
            this.options = options;
        }

        @Override
        public String toString() {
            return "Woman{" +
                    "options='" + options + '\'' +
                    "} " + super.toString();
        }
    }

    public static class Child extends Human {
        private Man father;
        private Woman mother;
        private int age;
        private Long num;
        private Date [] dates;

        public Child() {
        }

        public Child(Gender gender, String name, Date birthDate, Man father, Woman mother, int age, long num) {
            super(gender, name, birthDate);
            this.father = father;
            this.mother = mother;
            this.age = age;
            this.num = num;
            this.dates = new Date[]{new Date(), new Date(), new Date()};
        }

        @Override
        public String toString() {
            return "Child{" +
                    "father=" + father +
                    ", mother=" + mother +
                    ", age=" + age +
                    ", num=" + num +
                    "} " + super.toString();
        }
    }

    private Child object;

    @Before
    public void setUp() {
        Man father = new Man("Bobby", new Date(), "father");
        Woman mother = new Woman("Kat", new Date(), "mother");
        object = new Child(Gender.male, "Stive", new Date(), father, mother, 10, 5);
        Format.put(Gender.class, new EnumFormat(Gender.class));
    }

    @Test
    public void testWrite() throws Exception {
        ObjectWriter writer = new ObjectWriter();
        Properties props = writer.write(object, "Stive");
        StringWriter text = new StringWriter();
        props.store(text, "test");
        System.out.println(text.getBuffer());
    }

    @Test
    public void testRead() throws IOException {
        ObjectWriter writer = new ObjectWriter();
        String path = "Stive";
        Properties props = writer.write(object, path);
        System.out.println(toString(props));
        ObjectReader reader = new ObjectReader();
        Child Stive = reader.read(object.getClass(), path, props);
        System.out.println(Stive);
        assertTrue(Stive.toString().equals(object.toString()));
    }

    private String toString(Properties props) throws IOException {
        StringWriter text = new StringWriter();
        props.store(text, "test");
        return text.getBuffer().toString();
    }
}