package ru.manufacture.util.io;

import java.io.File;
import java.io.IOException;
import org.junit.Test;

public class PathTypeTest {
    @Test
    public void execute() throws IOException {
        String jarFile = "junit-4.12.jar";
        String classFile = "org\\junit\\Test.class";

        String jarPath = getJarPath(jarFile);
        boolean exists = PathType.JAR.exists(jarPath, classFile);
        System.out.println(exists);
        byte[] bytes = PathType.JAR.read(jarPath, classFile);
        System.out.println(bytes);
        String projectHome = new File("").getAbsolutePath();
        exists = PathType.DIRECTORY.exists(projectHome, "src/test/resources/process/process[1].xml");
        System.out.println(exists);
    }

    private String getJarPath(String jarFile) {
        String classPath = System.getProperty("java.class.path");
        int indx = classPath.indexOf(jarFile);
        String jarPath = classPath.substring(0, indx + jarFile.length());
        jarPath = jarPath.substring(jarPath.lastIndexOf(";") + 1);
        return jarPath;
    }
}