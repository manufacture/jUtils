package ru.manufacture.util.io;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;

public class LocationTest {

    @Test
    public void testCopyTo() throws Exception {
        String userHome = System.getProperty("user.home");
        String someJar = ".jaction\\commons\\commons-lang-2.6.jar";
        Path destDir = Paths.get(userHome, "test-copy-to");

        Location location = new Location(PathType.DIRECTORY, userHome, someJar);
        location.copyTo(destDir);

        location = new Location(PathType.JAR, userHome + File.separator + someJar, "org\\apache\\commons\\lang\\NumberUtils.class");
        location.copyTo(destDir);
    }
}