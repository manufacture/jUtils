package ru.manufacture.util.reflect;

import java.lang.reflect.Field;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class ReflectSupportTest extends Assert {
    private final String text = "test";

    @Test
    public void testGetFields() throws Exception {
        List<Field> fields = ReflectSupport.getFields(getClass());
        System.out.println(fields);
        Field text = null;
        for (Field field : fields) {
            if (field.getName().equals("text")) {
                text = field;
                break;
            }
        }
        assertTrue(null != text);

        System.out.println(text.get(this));
        text.set(this, "new value");
        System.out.println(text.get(this));
    }
}