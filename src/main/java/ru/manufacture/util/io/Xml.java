package ru.manufacture.util.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Path;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

/**
 * @author Degtyarev Roman
 * @date 19.06.2015.
 */
public class Xml {
    public static Document read(InputStream xml) throws DocumentException {
        SAXReader reader = new SAXReader();
        return reader.read(xml);
    }

    public static Document read(String xml) throws DocumentException {
        SAXReader reader = new SAXReader();
        return reader.read(new StringReader(xml));
    }

    public static Document read(Path path) throws DocumentException {
        SAXReader reader = new SAXReader();
        return reader.read(path.toFile());
    }

    public static void validate(InputStream xml, InputStream ...xsd) throws IOException, SAXException {
        Source xmlSrc = new StreamSource(xml);
        Source[] xsdSrc = new Source[xsd.length];
        for (int i = 0; i < xsd.length; i++) {
            xsdSrc[i] = new StreamSource(xsd[i]);
        }
        validate(xmlSrc, xsdSrc);
    }

    public static void validate(Path xml, Path ... xsd) throws IOException, SAXException {
        Source xmlSrc = new StreamSource(new FileInputStream(xml.toFile()));
        Source[] xsdSrc = new Source[xsd.length];
        for (int i = 0; i < xsd.length; i++) {
            xsdSrc[i] = new StreamSource(new FileInputStream(xsd[i].toFile()));
        }
        validate(xmlSrc, xsdSrc);
    }

    public static void validate(Source xml, Source ...xsd) throws SAXException, IOException {
        Schema schema = schema(xsd);
        Validator validator = schema.newValidator();
        validator.validate(xml);
    }

    public static Schema schema(Source ... xsd) throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return factory.newSchema(xsd);
    }
}
