package ru.manufacture.util.io.props;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import ru.manufacture.types.format.Format;
import ru.manufacture.util.reflect.ReflectSupport;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class ObjectWriter {
    public static final String ARRAY_DELIMITER = "|||";

    private Properties props;

    public Properties write(Object object, String alias) {
        synchronized (this) {
            props = new Properties();
            writeObject(object, null == object ? null : object.getClass(), alias);
            return props;
        }
    }

    private void writeObject(Object object, Class type, String path) {
        if (null == object) {
            props.setProperty(path, "");
            return;
        }
        if (type.isArray()) {
            if (!Format.contains(type.getComponentType())) {
                throw new IllegalArgumentException("Array item type not supported: " + path + " type " + type.getComponentType());
            }
            writeArray((Object[]) object, path);
            return;
        }
        if (Collection.class.isAssignableFrom(type)) {
            throw new IllegalArgumentException("Collection objects not supported: " + path);
        }
        if (Map.class.isAssignableFrom(type)) {
            throw new IllegalArgumentException("Map objects not supported: " + path);
        }
        if (type.isEnum() || type.isPrimitive() || Format.contains(type)) {
            props.setProperty(path, Format.toString(object));
            return;
        }

        List<Field> fields = ReflectSupport.getFields(type);
        for (Field field : fields)
            try {
                writeObject(field.get(object), field.getType(), path + "." + field.getName());
            } catch (IllegalAccessException ignore) {
            }
    }

    private void writeArray(Object[] objects, String path) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < objects.length; i++) {
            if (i > 0) {
                buffer.append(ARRAY_DELIMITER);
            }
            buffer.append(Format.toString(objects[i]));
        }
        props.setProperty(path, buffer.toString());
    }
}
