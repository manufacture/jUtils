package ru.manufacture.util.io.props;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import ru.manufacture.types.format.Format;
import ru.manufacture.util.reflect.ReflectSupport;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class ObjectReader {
    private Properties props;

    public <Type> Type read(Class<Type> type, String alias, Properties properties) {
        synchronized (this) {
            this.props = properties;
            return readObject(type, alias);
        }
    }

    private <Type> Type readObject(Class<Type> type, String path) {
        if (type.isArray()) {
            if (!Format.contains(type.getComponentType())) {
                throw new IllegalArgumentException("Array item type not supported: " + path + " type " + type.getComponentType());
            }
            return (Type) readArray(type.getComponentType(), path);
        }
        if (Collection.class.isAssignableFrom(type)) {
            throw new IllegalArgumentException("Collection objects not supported: " + path);
        }
        if (Map.class.isAssignableFrom(type)) {
            throw new IllegalArgumentException("Map objects not supported: " + path);
        }
        if (type.isEnum() || type.isPrimitive() || Format.contains(type)) {
            String strValue = props.getProperty(path);
            return Format.toObject(type, strValue);
        }

        Type object = newInstance(type);
        List<Field> fields = ReflectSupport.getFields(type);
        for (Field field : fields) {
            Object fieldValue = readObject(field.getType(), path + "." + field.getName());
            try {
                field.set(object, fieldValue);
            } catch (IllegalAccessException ignore) {
            }
        }
        return object;
    }

    private <Type> Type[] readArray(Class<Type> type, String path) {
        String string = props.getProperty(path);
        String[] strings = StringUtils.split(string, ObjectWriter.ARRAY_DELIMITER);
        Type[] array = (Type[]) Array.newInstance(type, strings.length);
        for (int i = 0; i < strings.length; i++) {
            array[i] = Format.toObject(type, strings[i]);
        }
        return array;
    }

    private <Type> Type newInstance(Class<Type> type) {
        try {
            return type.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Can't create new object instance", e);
        }
    }
}
