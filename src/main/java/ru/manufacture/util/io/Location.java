package ru.manufacture.util.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;

/**
 * @author Degtyarev Roman
 * @date 22.10.2015.
 */
public class Location {
    private PathType type;
    private String path;
    private String resource;

    public Location(PathType type, String path, String resource) {
        this.type = type;
        this.path = path;
        this.resource = resource;
    }

    public PathType getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    public String getResource() {
        return resource;
    }

    public InputStream getResourceAsStream() throws IOException {
        return type.getInputStream(path, resource);
    }

    public URL getResourceAsUrl() {
        return type.toUrl(path, resource);
    }

    public byte[] getContent() throws IOException {
        return type.read(path, resource);
    }

    public Path copyTo(Path destDir) throws IOException {
        return type.copyTo(path, resource, destDir);
    }

    @Override
    public String toString() {
        return "Location{" +
                "type=" + type +
                ", path='" + path + '\'' +
                ", resource='" + resource + '\'' +
                '}';
    }
}
