package ru.manufacture.util.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Path;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class Io {

    private static final int BUFFER_SIZE = 1024;

    public static void flow(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        do {
            int n = in.read(buffer, 0, BUFFER_SIZE);
            if (n < 0) break;
            out.write(buffer, 0, n);
        } while (true);
        out.flush();
    }

    public static void flow(Reader in, Writer out) throws IOException {
        char[] buffer = new char[BUFFER_SIZE];
        do {
            int n = in.read(buffer, 0, BUFFER_SIZE);
            if (n < 0) break;
            out.write(buffer, 0, n);
        } while (true);
        out.flush();
    }

    public static void flow(InputStream in, Writer out, String encoding) throws IOException {
        try (InputStreamReader reader = new InputStreamReader(in, encoding)) {
            flow(reader, out);
        }
    }

    public static void flow(Reader in, OutputStream out, String encoding) throws IOException {
        try (OutputStreamWriter writer = new OutputStreamWriter(out, encoding)) {
            flow(in, writer);
        }
    }

    public static String read(InputStream inStream, String encoding) throws IOException {
        StringWriter string = new StringWriter();
        flow(inStream, string, encoding);
        inStream.close();
        return string.getBuffer().toString();
    }

    public static String read(Reader reader) throws IOException {
        StringWriter text = new StringWriter();
        flow(reader, text);
        reader.close();
        return text.getBuffer().toString();
    }

    public static String read(Path path, String encoding) throws IOException {
        return read(path.toFile(), encoding);
    }

    public static String read(File file, String encoding) throws IOException {
        try (InputStream in = new FileInputStream(file)) {
            return read(in, encoding);
        }
    }

    public static byte [] read(Path path) throws IOException {
        return read(path.toFile());
    }

    public static byte [] read(File file) throws IOException {
        try (InputStream in = new FileInputStream(file);
             ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            flow(in, out);
            return out.toByteArray();
        }
    }

    public static void write(File file, String text, String encoding) throws IOException {
        try (OutputStream out = new FileOutputStream(file);
             Reader in = new StringReader(text)
        ) {
            flow(in, out, encoding);
        }
    }
}
