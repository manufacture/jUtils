package ru.manufacture.util.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Degtyarev Roman
 * @date 15.10.2015.
 */
public enum PathType {
    DIRECTORY {
        @Override
        public byte[] read(String dirPath, String fileName) throws IOException {
            Path path = Paths.get(dirPath, fileName);
            return Io.read(path);
        }

        @Override
        public boolean exists(String dirPath, String fileName) {
            Path path = Paths.get(dirPath, fileName);
            return Files.exists(path);
        }

        @Override
        public URL toUrl(String dirPath, String fileName) {
            Path path = Paths.get(dirPath, fileName);
            try {
                return path.toUri().toURL();
            } catch (MalformedURLException e) {
                throw new IllegalStateException("Can't get path URL: " + path, e);
            }
        }

        @Override
        public InputStream getInputStream(String dirPath, String fileName) throws IOException {
            Path path = Paths.get(dirPath, fileName);
            return new FileInputStream(path.toFile());
        }

        @Override
        public Path copyTo(String dirPath, String fileName, Path destDir) throws IOException {
            Path destPath = destDir.resolve(fileName);
            if (!Files.exists(destPath.getParent())) {
                Files.createDirectories(destPath.getParent());
            }
            try (InputStream in = getInputStream(dirPath, fileName);
                 OutputStream out = new FileOutputStream(destPath.toFile())
            ) {
                Io.flow(in, out);
            }
            return destPath;
        }

        @Override
        public String normalize(String path) {
            return null == path ? null : path.replace("\\", File.separator).replace("/", File.separator);
        }
    },
    JAR {
        @Override
        public byte[] read(String jarFilePath, String jarEntryName) throws IOException {
            try (JarFile jarFile = new JarFile(jarFilePath)) {
                JarEntry entry = jarFile.getJarEntry(normalize(jarEntryName));
                InputStream entryStream = jarFile.getInputStream(entry);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Io.flow(entryStream, out);
                return out.toByteArray();
            }
        }

        @Override
        public boolean exists(String jarFilePath, String jarEntryName) {
            Path path = Paths.get(jarFilePath);
            if (!Files.exists(path)) {
                return false;
            }
            try (JarFile jarFile = new JarFile(path.toFile())) {
                JarEntry entry = jarFile.getJarEntry(normalize(jarEntryName));
                return null != entry;
            } catch (Exception ignore) {
                return false;
            }
        }

        @Override
        public URL toUrl(String jarFilePath, String jarEntryName) {
            try {
                return new URL("jar:file:/" + normalize(jarFilePath) + "!/" + normalize(jarEntryName));
            } catch (MalformedURLException e) {
                throw new IllegalStateException("Can't get JAR entry URL: " + jarFilePath + " " + jarEntryName, e);
            }
        }

        @Override
        public InputStream getInputStream(String jarFilePath, String jarEntryName) throws IOException {
            try (JarFile jarFile = new JarFile(jarFilePath)) {
                JarEntry entry = jarFile.getJarEntry(normalize(jarEntryName));
                InputStream entryStream = jarFile.getInputStream(entry);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Io.flow(entryStream, out);
                return new ByteArrayInputStream(out.toByteArray());
            }
        }

        @Override
        public Path copyTo(String jarFilePath, String jarEntryName, Path destDir) throws IOException {
            Path destPath = destDir.resolve(DIRECTORY.normalize(jarEntryName));
            if (!Files.exists(destPath.getParent())) {
                Files.createDirectories(destPath.getParent());
            }
            try (JarFile jarFile = new JarFile(jarFilePath);
                 OutputStream out = new FileOutputStream(destPath.toFile())
            ) {
                JarEntry entry = jarFile.getJarEntry(normalize(jarEntryName));
                InputStream entryStream = jarFile.getInputStream(entry);
                Io.flow(entryStream, out);
            }
            return destPath;
        }

        @Override
        public String normalize(String jarEntryName) {
            return null == jarEntryName ? null : jarEntryName.replace("\\", "/");
        }
    };

    public static PathType of(String path) {
        boolean isJar = path.toUpperCase().endsWith(".JAR");
        return isJar ? JAR : DIRECTORY;
    }

    public abstract byte [] read(String path, String resource) throws IOException;

    public abstract boolean exists(String path, String resource);

    public abstract URL toUrl(String path, String resource);

    public abstract InputStream getInputStream(String path, String resource) throws IOException;

    public abstract Path copyTo(String srcPath, String srcResource, Path destDir) throws IOException;

    public abstract String normalize(String path);
}
