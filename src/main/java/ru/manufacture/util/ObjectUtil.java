package ru.manufacture.util;

/**
 * @author Degtyarev Roman
 * @date 20.10.2015.
 */
public class ObjectUtil {
    public static <Type> Type getInstanceBySystemProperty(Class<? extends Type> type, Class<? extends Type> defaultType) {
        String className = System.getProperty(type.getName(), defaultType.getName());
        try {
            Class<?> aClass = Class.forName(className);
            return (Type) aClass.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Class initialization fails: " + className, e);
        }
    }
}
