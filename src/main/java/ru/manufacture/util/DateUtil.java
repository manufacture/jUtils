package ru.manufacture.util;

import java.util.Date;
import ru.manufacture.types.format.Format;

/**
 * @author Degtyarev Roman
 * @date 16.09.2015.
 */
public class DateUtil {
    public static final Date POSITIVE_INFINITY_DATE = Format.toObject(Date.class, "01.01.2999");
    public static final Date NEGATIVE_INFINITY_DATE = Format.toObject(Date.class, "01.01.0001");
}
