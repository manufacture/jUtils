package ru.manufacture.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.junit.Assert;

/**
 * @author Degtyarev Roman
 * @date 22.12.2015.
 */
public class NumeralSystem {
    private String validChars;

    public static NumeralSystem binary() {
        return new NumeralSystem("01");
    }

    public static NumeralSystem decimal() {
        return new NumeralSystem("0123456789");
    }

    public static NumeralSystem octal() {
        return new NumeralSystem("01234567");
    }

    public static NumeralSystem hex() {
        return new NumeralSystem("0123456789ABCDEF");
    }

    public NumeralSystem(String validChars) {
        this.validChars = validChars.toUpperCase();
    }

    public int getRadix() {
        return validChars.length();
    }

    public BigInteger toDec(String value) {
        int [] chars = new int[value.length()];
        int i = 0;
        for (char c : value.toUpperCase().toCharArray()) {
            if (!validChars.contains("" + c))
                throw new IllegalArgumentException("Invalid character: '" + c + "'");
            chars[i++] = validChars.indexOf(c);
        }
        return toDecValue(chars);
    }

    public String fromDec(long decValue) {
        int[] value = fromDecValue(decValue);
        StringBuffer bufer = new StringBuffer();
        for (int c : value) {
            bufer.append(validChars.charAt(c));
        }
        return bufer.toString();
    }

    private BigInteger toDecValue(int [] chars) {
        BigInteger result = new BigInteger("0");
        BigInteger tempvalue = new BigInteger("1");
        for (int i = chars.length - 1; i >= 0; i--) {
            result = result.add(tempvalue.multiply(BigInteger.valueOf(chars[i])));
            tempvalue = tempvalue.multiply(BigInteger.valueOf(getRadix()));
        }
        return result;
    }

    private int[] fromDecValue(long decValue) {
        List<Integer> resultList = new ArrayList<>();
        while (decValue != 0) {
            resultList.add((int)(decValue % getRadix()));
            decValue /= getRadix();
        }
        Collections.reverse(resultList);
        int [] array = new int[resultList.size()];
        int i = 0;
        for (int integer : resultList) {
            array[i++] = integer;
        }
        return array;
    }

    public static void main(String[] args) {
        Assert.assertTrue(NumeralSystem.binary().toDec("0101").longValue() == 5);
        Assert.assertTrue(NumeralSystem.binary().fromDec(10).equals("1010"));
        Assert.assertTrue(NumeralSystem.decimal().fromDec(75).equals("75"));
        Assert.assertTrue(NumeralSystem.decimal().toDec("123").longValue() == 123);
        Assert.assertTrue(NumeralSystem.hex().toDec("FF").longValue() == 255);
        Assert.assertTrue(NumeralSystem.hex().fromDec(201).equals("C9"));
    }
}
