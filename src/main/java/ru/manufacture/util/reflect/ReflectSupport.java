package ru.manufacture.util.reflect;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isStatic;
import static java.lang.reflect.Modifier.isTransient;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class ReflectSupport {
    public enum FieldSelector {
        SET_ACCESSIBLE,
        INHERITED,
        TRANSIENT,
        STATIC,
        FINAL
    }

    public static List<Field> getFields(Class type) {
        return getFields(type, FieldSelector.SET_ACCESSIBLE, FieldSelector.INHERITED, FieldSelector.FINAL);
    }

    public static List<Field> getFields(Class<?> type, FieldSelector ... selectors) {
        List<Field> result = new LinkedList<Field>();
        List<FieldSelector> selectorList = Arrays.asList(selectors);
        Class c = type;
        while (c != null) {
            for (Field field : c.getDeclaredFields()) {
                if (field.getName().startsWith("this$")) continue;
                int modifiers = field.getModifiers();
                if (isStatic(modifiers) && !selectorList.contains(FieldSelector.STATIC)) continue;
                if (isTransient(modifiers) && !selectorList.contains(FieldSelector.TRANSIENT)) continue;
                if (isFinal(modifiers) && !selectorList.contains(FieldSelector.FINAL)) continue;
                if (selectorList.contains(FieldSelector.SET_ACCESSIBLE)) {
                    field.setAccessible(true);
                }
                result.add(field);
            }
            if (!selectorList.contains(FieldSelector.INHERITED)) break;
            c = c.getSuperclass();
        }
        return result;
    }
}
