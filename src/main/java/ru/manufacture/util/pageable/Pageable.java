package ru.manufacture.util.pageable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
public abstract class Pageable implements Serializable {
    private long currentPage;

    protected Pageable(Long currentPage) {
        this.currentPage = null == currentPage ? 1L : currentPage;
        if (this.currentPage < 1L) {
            this.currentPage = 1L;
        }
    }

    /**
     * возвращает кол-во записей на странице
     */
    public abstract int getPageSize();

    /**
     * возвращает кол-во ссылок на страницы
     */
    public abstract int getPageDiapasonSize();

    /**
     * возвращает кол-во строк, которые надо побить на страницы
     */
    public abstract long getTotalRows();

    public long getCurrentPage() {
        return currentPage;
    }

    /**
     * вычисляет кол-во страниц
     */
    public long getPageCount() {
        long total = getTotalRows();
        long pageCount = total / getPageSize();
        if (pageCount * getPageSize() < total) {
            pageCount++;
        }
        return pageCount;
    }

    public long getFromRow() {
        return getPageSize() * (getCurrentPage() - 1) + 1;
    }

    public long getToRow() {
        return getPageSize() * getCurrentPage();
    }

    public List<Long> getPagesToDisplay() {
        long pageSet = getCurrentPage() / getPageDiapasonSize();
        long toPage;
        if (getCurrentPage() % getPageDiapasonSize() == 0) {
            toPage = pageSet * getPageDiapasonSize();
        } else {
            toPage = (pageSet + 1) * getPageDiapasonSize();
        }
        long fromPage = toPage - getPageDiapasonSize() + 1;
        toPage = Math.min(getPageCount(), toPage);
        List<Long> pageDiapason = new ArrayList<>();
        for (long i = fromPage; i <= toPage; i++) {
            pageDiapason.add(i);
        }
        return pageDiapason;
    }
}
