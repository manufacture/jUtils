package ru.manufacture.util.pageable;

/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */

import java.util.List;

public class SimplePageable<DataType extends PageableMetadata> extends Pageable {
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int DEFAULT_PAGE_DIAPASON = 10;

    private List<DataType> data;
    private int pageSize;

    public SimplePageable(Long currentPage) {
        this(currentPage, DEFAULT_PAGE_SIZE);
    }

    public SimplePageable(Long currentPage, int pageSize) {
        super(currentPage);
        this.pageSize = pageSize;
    }

    @Override
    public int getPageSize() {
        return pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE;
    }

    @Override
    public int getPageDiapasonSize() {
        return DEFAULT_PAGE_DIAPASON;
    }

    @Override
    public long getTotalRows() {
        return isEmpty() ? 0 : getData().get(0).getTotalRowsCount();
    }

    private boolean isEmpty() {
        return null == getData() || getData().isEmpty();
    }

    public List<DataType> getData() {
        return data;
    }

    public void setData(List<DataType> data) {
        this.data = data;
    }
}
