package ru.manufacture.util.pageable;

/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
public interface PageableMetadata {
    long getTotalRowsCount();
}
