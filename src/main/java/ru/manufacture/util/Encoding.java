package ru.manufacture.util;

/**
 * @author Degtyarev Roman
 * @date 21.10.2015.
 */
public enum Encoding {
    UTF8("UTF-8"), CP1251("cp1251");

    public static final Encoding DEFAULT = UTF8;

    private final String code;

    Encoding(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }
}
