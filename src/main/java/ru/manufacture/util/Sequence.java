package ru.manufacture.util;

/**
 * @author Degtyarev Roman
 * @date 21.07.2015.
 */
public class Sequence {
    private long value;
    private long startValue;
    private long incValue;

    public Sequence() {
        this(1, 1);
    }

    public Sequence(long startValue, long incValue) {
        this.startValue = startValue;
        this.incValue = incValue;
        this.value = startValue;
    }

    public long getValue() {
        return value;
    }

    public long getNext() {
        value += incValue;
        return value;
    }

    public long getStartValue() {
        return startValue;
    }

    public long getIncValue() {
        return incValue;
    }
}
